

    /* Nella nazione ci sono N città, che vanno da N a N -1, connesse bidirezionalmente da N -1 strade,
    E' possibile viaggiare da una città all'altra seguendo le varie strade.

    Le strade posson prendere fuoco, il fuoco arriva e passa da città in città,
    ma in ogniuna di queste c'è una stazione dei vigili del fuoco che può spegnere il fuoco
    distruggendo una strada a caso così salvando una città.

    noti che non appena il fuoco raggiunge una città, quella città non può più essere salvata.
È noto che in caso di incendio, i vigili del fuoco si coordinano e agiscono in modo ottimale per
massimizzare il numero totale di città salvate nel paese. Per ogni città del paese, vorresti sapere
quante città verranno distrutte se un incendio inizia in quella città.

 */
/*

void brucia()
{


}

// Creo le città.
Struct Citta{
    int numeroz;
    bool dis = false;
}

// Creo le strade
Struct Strade{
    int sCitta;
}
/*Struct cittaNestata{
    int numero;
    int strada;

}*/

/*

int main(){
    int numero;
    printf("Scrivere numero\n");
    scanf("%d", &numero);

//Riempio l'array di città
    struct Citta citta[&numero];
    for(int i = 0; i < numero; i++)
    {
        citta[i].numeroz(i);
        
    }
    
    //Creo le strade come numero meno uno
    struct Strade strade[&numero - 1];
    for(int i = 0; i < numero - 1; i++)
    {
        strade[i].sCitta(i);
        
    }

    /*
    Devo interconnettere le strade seguendo uno schema ad albero.
    Ad esempio:
        0
    1       2
3       4       5
    !!NON SO COME SI FACCIA!!
    Idee:
    Posso inter connettere le città con le strade ad esempio:
        0
    s1      s2
    1       2

    Se strada due == fuoco distruggi citta[numeroStrada]
    */
//Posso provare con un nested for ma non credo funzioni.
//Abbanonato provo in altro modo
/*for(int i = 0; i < sizeof(citta); i++)
{

    for(int j= 0; j < sizeof())
    {

    }
}

return 0;
}*/

// Esercizio rifatto.
/*
#include <stdio.h>
#include <stdbool.h>

// Definizione delle strutture
typedef struct {
    int numeroz;
    bool dis;
    int strade;
} Citta;

void brucia(Citta[] c){
for(int i = 0; i < sizeof(c), i++){

    
}

}

int main() {
    int numero;
    printf("Inserisci un numero: ");
    scanf("%d", &numero);

    Citta citta[numero];
    for(int i = 0; i < numero; i++) {
        citta[i].numeroz = i;
        citta[i].dis = false;
    }

    int strade[numero - 1];
    for(int i = 0; i < numero - 1; i++){

        strade[i] = i;
        citta[i].strade = strade[i];
    }




    return 0;
}
*/

//Esercizio rifatto numero 3.

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

void bruciaEstampa(bool citta[], int n) {
    const int min = 0;
    const int max = 10;

    citta[0] = false;

    for (int i = 1; i < n; i++) {
        int bonsai = rand() % (max - min + 1) + min;
        if (bonsai % 2 == 0) {
            citta[i] = false;
        }
    }

    printf("Ecco le città che si sono salvate: \n");
    for (int i = 0; i < n; i++) {
        if (citta[i]) {
            printf("Città %d\n", i);
        }
    }
}

int main() {
    int d;
    printf("Inserisci quante città vuoi avere: ");
    scanf("%d", &d);

    bool citta[d];

    for (int i = 0; i < d; i++) {
        citta[i] = true;
    }

    bruciaEstampa(citta, d);

    return 0;
}
